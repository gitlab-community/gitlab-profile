# frozen_string_literal: true

require_relative '../../bin/update_onboarding_issue_label'

RSpec.describe UpdateOnboardingIssueLabel do
  before do
    stub_env('CI_API_V4_URL', 'https://gitlab.com/api/v4')
    stub_env('UPDATE_ONBOARDING_ISSUE_LABEL_GITLAB_API_TOKEN', token)
    stub_env('ISSUE_ASSIGNEE_ID', '999')
    stub_env('ISSUE_COMMENTER_ID', '777')
    stub_env('ISSUE_IID', issue_iid)
    stub_env('DRY_RUN', '0')
  end

  let(:issue_iid) { '123' }
  let(:token) { 'glpat-xxxxxxxxxxxxxxxxxxxx' }

  describe '#execute', vcr: { cassette_name: 'UpdateOnboardingIssueLabel/maintainers' } do
    subject(:execute) { described_class.new.execute }

    let(:gitlab_client) { GitlabClientHelper.new(token) }

    before do
      allow(GitlabClientHelper).to receive(:new).and_return(gitlab_client)

      allow(gitlab_client).to receive(:create_issue_note)
    end

    context 'when assignee' do
      before do
        stub_env('ISSUE_COMMENTER_ID', '999')
      end

      it 'adds a note adding the ~updated label' do
        execute

        expect(gitlab_client).to have_received(:create_issue_note).with(
          described_class::ONBOARDING_PROJECT_ID,
          issue_iid,
          '/label ~updated'
        )
      end

      context 'when dry run' do
        before do
          stub_env('DRY_RUN', '1')
        end

        it 'does not add a note' do
          execute

          expect(gitlab_client).not_to have_received(:create_issue_note)
        end
      end
    end

    context 'when maintainer' do
      before do
        stub_env('ISSUE_COMMENTER_ID', '12687636')
      end

      it 'adds a note removing the ~updated label' do
        execute

        expect(gitlab_client).to have_received(:create_issue_note).with(
          described_class::ONBOARDING_PROJECT_ID,
          issue_iid,
          '/unlabel ~updated'
        )
      end

      context 'when dry run' do
        before do
          stub_env('DRY_RUN', '1')
        end

        it 'does not add a note' do
          execute

          expect(gitlab_client).not_to have_received(:create_issue_note)
        end
      end
    end

    context 'when someone else' do
      it 'does not add a note' do
        execute

        expect(gitlab_client).not_to have_received(:create_issue_note)
      end
    end
  end
end
