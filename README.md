# GitLab Community Forks

[[_TOC_]]

## About

A collection of GitLab project forks aimed at improving the community contribution experience.

## Why

- Promote collaboration:
  - Community members and team members can work on the same merge request (pushing commits to each other's branches).
  - They can also take over stuck/idle merge requests (from each other and from team members).
- Remove blockers:
  - Personal CI minute and storage quotas do not apply to the community forks as they are part of the GitLab for Open Source Program which provides GitLab Ultimate tier benefits (including larger quotas).
- Improve efficiency:
  - There is no longer a need to create a personal fork to get started contributing.
  - Danger runs automatically, without the need to configure personal access tokens and CI variables.
  - Pull mirroring keeps the community forks up to date, without regular manual rebasing.

## How to

### Request access to community forks

Click [here](https://gitlab.com/groups/gitlab-community/community-members/-/group_members/request_access)
to request access to the community forks.

### Onboarding issue

When you request access to the community forks you will receive an onboarding issue in the
[onboarding project](https://gitlab.com/gitlab-community/community-members/onboarding) within 5 minutes.
This issue is designed to welcome you into the community, track the status of your access request,
and offer an optional checklist of onboarding steps to start contributing to GitLab.

#### Access request status

- The ~"access-request::pending" label is added by default to all onboarding issues awaiting review.
- The ~"access-request::awaiting-information" label is applied by the reviewer if more information is requested.
- The ~"access-request::approved" label is applied by the reviewer after the access request is approved.
- The ~"access-request::closed" label is applied by the reviewer when closing without approval.

#### Closing onboarding issues

You are free to close out your own onboarding issue whenever you are finished.
Issues will automatically be closed after 90 days with no activity but you will be invited to reopen the issue
if you would like to continue working through the onboarding steps or have questions.

### Approve an access request

Members of the [@gitlab-community/maintainers group](https://gitlab.com/groups/gitlab-community/maintainers/-/group_members?with_inherited_permissions=exclude)
manage access requests.
We use a subgroup to allow additional administrators to manage access without becoming owners of the top level group.

The reviewer will update the [access request status](#access-request-status) label in the onboarding issue.

- If you have a public profile, we recommend adding a short bio and links to your socials which
  can help speed up review time.
- Existing merge requests or comments on issues/merge requests can provide an indicator for us to
  [assume positive intent](https://about.gitlab.com/handbook/values/#assume-positive-intent).
- If there is not enough profile activity to approve or any indication of spam/malintent, the reviewer
  will engage with the contributor in their onboarding issue and request more information before approving the request.
- Security researchers and HackerOne participants are not granted access to the community forks
  but are welcome to report on publicly accessible items to GitLab security.

#### Triage and monitor access requests

Community maintainers use an [onboarding issues report](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/734)
to track onboarding issues which may require attention.
The report uses [GLQL](https://docs.gitlab.com/ee/user/glql/index.html#glql-views) to keep itself updated with the following issue type:

- Issues labeled ~"access-request::pending".
  - Maintainers should review the request by following the [approve an access request](#approve-an-access-request) process.
- Issues labeled ~"updated".
  - Maintainers should check these issues where the contributor was the last user to reply.
  - Reply to the contributor and/or remove the ~"updated" label if no action is required.

### GitLab Duo

As a benefit of being a GitLab Community Contributor, you get complimentary access to GitLab Duo Enterprise through the community forks. 
With Code Suggestions, Chat, Root Cause Analysis and more AI-powered features, [GitLab Duo](https://docs.gitlab.com/ee/user/gitlab_duo/)
helps to boost your efficiency and effectiveness by reducing the time required to write and understand code and pipelines.

- Write secure code more efficiently and accelerate cycle times by taking care of repetitive, routine coding tasks.
- Generate tests, explain code, refactor efficiently, and chat directly in your IDE or web interface.
- Discover or recall Git commands when and where you need them.
- Quickly resolve CI/CD pipeline issues with AI-assisted root cause analysis for CI/CD job failures.

If you're already a member of the community forks you can start using GitLab Duo today. 

If you haven't requested access yet, [join the community forks by clicking here](https://gitlab.com/groups/gitlab-community/community-members/-/group_members/request_access)
to get GitLab Duo.

### Work in a community fork

1. Clone the fork.
1. Create a new branch.
1. Push the branch.
1. Create merge request against the canonical project (not the community fork).

#### GitLab project specifics

- For **existing GitLab Development Kit (GDK) installations**:
  - **The following command is destructive**: unpushed commits, stashes or files not tracked by Git inside the `gitlab` directory will be lost.
    Make sure to push all branches and save your stashes and untracked files before proceeding. \
    `rm -rf gitlab` from inside your `gitlab-development-kit` folder.
  - Then `git clone https://gitlab.com/gitlab-community/gitlab.git`.

  > **Note**: Anything that lives outside the `gitlab` folder, for example, the database, won't get affected by this.

- For **new GitLab Development Kit (GDK) installations**:
  - Enter `https://gitlab.com/gitlab-community/gitlab.git` when prompted for the GitLab repo URL.

### Request a new community fork

Create an issue in the issue tracker and use the `Fork request` [issue template](https://gitlab.com/gitlab-community/meta/-/issues/new?issuable_template=Fork%20request).

### Team member process for creating a new community fork

1. [Create subgroups](#create-and-update-subgroup) if required.
1. [Fork the project](#fork-the-project).
1. [Update the project settings](#update-the-project-settings).
   1. [Check and configure Container Registry](#container-registry) if necessary.
   1. [Check and configure Package Registry](#package-registry) if necessary.
1. [Configure pull mirroring](#configure-pull-mirroring).
1. [Add project badge](#add-project-badge).
1. Celebrate the new fork :tada:

#### Create and update subgroup

The community fork standard is to create a structure matching the full path of the upstream project.
For example, the [container-scanning CI/CD component project](https://gitlab.com/components/container-scanning)
is forked from `components/container-scanning` to [`gitlab-community/components/container-scanning`](https://gitlab.com/gitlab-community/components/container-scanning).

When creating a new subgroup, change the following settings in **Permissions and group features**:

- Set the **Group Wiki** to **Disabled**.
- Set **Roles allowed to create projects** to **Maintainers**
- Uncheck **Users can request access**.

#### Fork the project

The `gitlab-community` group structure should mimic the `gitlab-org` group structure.

- Select `gitlab-community`, or the relevant subgroup as the target namespace.
- Use the same name suffixed with `Community Fork`.
- Use the same slug as the upstream project.
- Use the description: `For information about community forks, checkout the [meta project](https://gitlab.com/gitlab-community/meta)`.
- Enable forking of only the default branch from the upstream project.
- Make a note of the Project ID for later.

#### Update the project settings

From **Settings > General > Visibility, project features, permissions** uncheck:

- **Users can request access**.
- **Issues**.
- **CVE requests**.
- **Forks**.
- [**Container registry**](#container-registry).
- **Requirements**.
- **Wiki**.
- **Snippets**.
- [**Package registry**](#package-registry).
- **Model experiments**.
- **Model registry**.
- **Pages**.
- **Monitor**.
- **Environments**.
- **Feature flags**.
- **Infrastructure**.
- **Releases**.

#### Container Registry

If you know the project uses the Container Registry in its pipeline, leave **Container registry** enabled,
and configure a Cleanup Policy at **Settings > Packages and registries > Edit cleanup rules** with the smallest values:

- Keep the most recent: **1 tag per image name**.
- Remove tags older than: **7 days**.

If you are unsure, disable it.

Failures like `invalid argument "some-image:some-tag" for "-t, --tag" flag: invalid reference format`
or `error checking push permissions -- make sure you entered the correct tag name, and that you are authenticated correctly, and try again: checking push permission for`
indicate that the Container Registry is required by the pipeline.

#### Package Registry

If you know the project uses the Package Registry in its pipeline, leave **Package registry** enabled.

If you are unsure, disable it.

Failures like `ERROR: Errors uploading some packages` mostly indicate that the Package Registry is required by the pipeline.

This error message will look different across all package managers or build tools, but they will normally contain some
error which occurred while trying the deployment of a package.

#### Configure pull mirroring

1. [Create a personal access token](https://docs.gitlab.com/ee/api/groups.html#create-personal-access-token-for-service-account-user)
   for the **Community Forks Service Account**.
   In this instance, `token` is a PAT for _your_ user. Make a note of the token generated for the pull mirroring step below.
   ```shell
   curl --request POST \
        --header "PRIVATE-TOKEN: token" \
        --data "scopes[]=api" \
        --data "name=service_accounts_token" \
        --data "expires_at=$(TZ=UTC-24 date +%Y-%m-%d)" \
        --url "https://gitlab.com/api/v4/groups/60717473/service_accounts/20710701/personal_access_tokens"
   ```
1. From **Settings > Repository > Protected branches** set it so:
   - **No one** can merge to `master`/`main`.
   - **No one** except `service_account_group_60717473_3f27c0b7cec844285a54381e5d91df38` can push and merge to `master`/`main`.
1. Enable pull mirroring using the project access token you made a note of in step 1.\
   _Pipe the output to `jq` if you want to make it easier to read, but a response containing the project JSON should indicate a success._
   ```shell
   curl --request PUT \
        --header "PRIVATE-TOKEN: token" \
        --data "import_url=https://gitlab.com/gitlab-org/<canonical>.git&mirror=true&mirror_trigger_builds=false&only_mirror_protected_branches=true&shared_runners_enabled=true&mirror_overwrites_diverged_branches=true" \
        --url 'https://gitlab.com/api/v4/projects/<community-fork-project-id>'
   ```
   **NOTE**: We setup pull mirroring using a service account to:
   - Avoid the mirroring activity flooding our personal activity feeds/profiles.
   - Ensure mirroring continues to work regardless of group membership (for example, if someone steps down as a maintainer).

#### Update canonical project

Reach out to a maintainer of the canonical project and ask them to:

##### Add project badge

[Add a project badge](https://docs.gitlab.com/ee/user/project/badges.html#add-a-badge):

- Name: `Community fork`.
- Link: <PATH_TO_COMMUNITY_FORK>.
- Badge image url: `https://img.shields.io/badge/Contribute-community%20fork-blue`.

##### Update project docs

Update the `README.md` or `CONTRIBUTING.md`
(linking to the community fork).

### Migrate an existing merge request into the community fork

Eventually, we hope all merge requests will come from the community fork, and this process will become redundant.
Until then, please follow the [takeover of the community merge request](https://docs.gitlab.com/ee/development/code_review.html#taking-over-a-community-merge-request) process,
recreating the branch in the respective [community fork repository](https://gitlab.com/gitlab-community).
For more details, please see [merge request ownership](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html#merge-request-ownership).

You do not need to add the canonical project or source fork, you can pull directly:

```shell
git fetch https://gitlab.com/fork-namespace/gitlab.git their-branch:our-name-for-branch
```

or

```shell
git fetch git@gitlab.com:fork-namespace/gitlab.git their-branch:our-name-for-branch
```

There is now a copy of their branch in your local `our-name-for-branch`.
Checkout the branch and push it to the community fork:

```shell
git checkout our-name-for-branch
git push --set-upstream origin our-name-for-branch
```

Follow the link in your terminal to create a new merge request in the community fork.
Make sure to link to the original merge request and/or issue. 

### Display Meta `README` on `gitlab-community` group page

- We have a [`gitlab-profile` project](https://gitlab.com/gitlab-community/gitlab-profile) to surface the `README` on the main
  [`gitlab-community` group](https://gitlab.com/gitlab-community) page.
  See the GitLab docs for how to [add a group `README`](https://docs.gitlab.com/ee/user/group/manage.html#add-group-readme).
- [Push mirroring](https://docs.gitlab.com/ee/user/project/repository/mirror/push.html) is configured for the `meta` project
  to ensure the `gitlab-profile` README is always up to date.
- [**Push rules**](https://docs.gitlab.com/ee/user/project/repository/push_rules.html#validate-branch-names) are configured
  to prevent new branches being created in the `gitlab-profile` project by mistake.
- To make changes to the `README`, create a merge request on the [`meta`](https://gitlab.com/gitlab-community/meta) project.

## Volunteering to support the community forks

By using the community forks, you are already being a great help, thank you!

There are no predefined criteria for becoming a maintainer.
Please reach out on [Discord](https://discord.gg/gitlab) if you would like to help out.

## Tips & tricks

### Checkout a branch from a different remote

Regardless of whether you work from the canonical project or a fork, if you simply
want to view/review changes from a merge request, the quickest method is to apply a diff:

```shell
git checkout master
curl diff url | git apply
```

For example:

```shell
curl https://gitlab.com/gitlab-org/gitlab/-/merge_requests/129370.diff | git apply
```

If you intend to push a commit, and want to checkout the full branch,
select **Code > Check out branch** from the merge request dropdown
and follow the instructions:

```shell
git fetch https://gitlab.com/<canonical group>/<canonical project>.git <remote-branch-name>:<remote-branch-name>
git checkout <remote-branch-name>
```

For example:

```shell
git fetch https://gitlab.com/gitlab-org/gitlab.git leetickett-test:leetickett-test
git checkout leetickett-test
```

If you frequently need to checkout upstream branches, you can add the canonical project/fork as a remote:

```shell
git remote add upstream https://gitlab.com/<canonical group>/<canonical project>.git
```

Then:

```shell
git fetch upstream
git checkout upstream/<remote-branch-name>
```

## Troubleshooting

### Failing pipelines

See [container registry](#container-registry) and [package registry](#package-registry) for potential reasons for pipeline failure.
