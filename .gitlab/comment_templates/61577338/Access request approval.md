<!-- REPLACE @USERNAME AND UNCOMMENT IT BEFORE SENDING THIS NOTE -->
Thanks <!-- @USERNAME --> :slight_smile:

I have activated your GitLab Duo Enterprise license :thumbsup:

I will close this issue and you will receive a welcome email with more information about using Duo.

If you need help or get stuck, feel free to ask in our Community [Discord](https://discord.gg/gitlab).

Please share your feedback on using GitLab Duo in the [community feedback issue](https://gitlab.com/gitlab-community/community-members/duo-access/-/issues/12).

/label duo-access::approved
/close
