<!-- Title: @username contributor onboarding -->
# 🌟 Welcome to your GitLab contribution journey! 🌟

/assign USERNAME
/label onboarding
/label access-request::pending

## 🎉 Welcome to the GitLab Community! 🎊

Hey USERNAME!
We're thrilled to have you join our amazing community of contributors! 🤝

This issue will guide you through your journey to becoming a GitLab contributor.
Our experienced maintainers (`@gitlab-community/maintainers`) will review your community forks access request and help you get started.

## 🚀 Your contribution adventure

Welcome to your GitLab contribution quest!
You've already taken the first step by requesting access - now let's explore what's next on your path to becoming a contributor!

## 🎬 First steps

- [X] Request access to the [community forks](https://gitlab.com/groups/gitlab-community/community-members/-/group_members/request_access)
  - Our maintainers will reach out if they need more info to [approve your request](https://gitlab.com/gitlab-community/meta/-/blob/main/README.md?ref_type=heads#approve-an-access-request)
  - Watch for the ~access-request::approved label once your request has been approved
- [ ] Say hello in the [community Discord](https://discord.gg/gitlab) `#contribute` channel 👋
  - [ ] Add your Discord ID to [your GitLab profile](https://gitlab.com/-/user_settings/profile) and earn points for your posts and replies!
- [ ] Experience the power of GitLab Duo - our AI suite including Code Suggestions, Chat, and more! 🤖
  -  You'll receive your GitLab Duo Enterprise license welcome email after your access is approved

## 📚 Master the basics

- [ ] Follow our [Tutorial: Make a GitLab Contribution](https://docs.gitlab.com/ee/development/contributing/first_contribution/)
- [ ] Join a [community pairing session](https://www.meetup.com/gitlab-virtual-meetups/) to learn from others 👥

## 🎯 Find your first issue

- [ ] Choose your first issue:
  - [First-time contributor issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=quick%20win%3A%3Afirst-time%20contributor&first_page_size=100) 🌱
  - [Quick wins](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=quick%20win&first_page_size=100)
  - [`quick win` docs](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=quick%20win&label_name%5B%5D=documentation&first_page_size=100) 📝
  - [`quick win` frontend](https://gitlab.com/groups/gitlab-org/-/issues/?sort=weight&state=opened&label_name%5B%5D=quick%20win&label_name%5B%5D=frontend&first_page_size=100) 🎨
  - [`quick win` backend](https://gitlab.com/groups/gitlab-org/-/issues/?sort=weight&state=opened&label_name%5B%5D=quick%20win&label_name%5B%5D=backend&first_page_size=100) ⚙️
- [ ] Add a comment to the issue you would like to work on along with any thoughts or questions
- [ ] Follow the implementation plan provided

## 🤝 Need help?

Type `@gitlab-bot help` in a merge request or issue to tag a [Merge Request Coach](https://docs.gitlab.com/ee/development/contributing/merge_request_coaches.html).

## 🛠️ Make your first contribution

- [ ] [Pick your development environment](https://docs.gitlab.com/ee/development/contributing/#choose-a-development-environment) and make your changes
- [ ] [Create your merge request](https://docs.gitlab.com/ee/development/contributing/#open-a-merge-request) using a community fork
- [ ] Follow the [review process](https://docs.gitlab.com/ee/development/contributing/#how-community-merge-requests-are-triaged) to get your work merged!

## 🏆 Level up your impact

- [ ] Explore the [GitLab Contributor Platform](https://contributors.gitlab.com/) to track your impact 📊
  - View your contribution metrics and achievements on your [personal dashboard](https://contributors.gitlab.com/users/me) 🏅
  - Track your progress on the [global contributor rankings](https://contributors.gitlab.com/leaderboard/me) 🏆
  - Showcase your organization's impact on the [company leaderboard](https://contributors.gitlab.com/organizations) 💫
    - Hint: [update your profile](https://gitlab.com/-/user_settings/profile), adding your company name under **Organization**
- [ ] Join our next [GitLab Hackathon](https://about.gitlab.com/community/hackathon/) 🎪
- [ ] Keep the momentum going:
  - Try another `quick win`
  - [Create your own issue](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#creating-an-issue) 💡
  - Explore our other exciting projects:
    - [cli](https://gitlab.com/gitlab-org/cli/-/issues/?sort=updated_desc&state=opened&first_page_size=100) 🖥️
    - [GitLab Terraform Provider](https://gitlab.com/gitlab-org/terraform-provider-gitlab/-/issues/?sort=updated_desc&state=opened&first_page_size=100) ⚡
    - [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/issues) 🏃
    - [gitaly](https://gitlab.com/gitlab-org/gitaly/-/issues/?sort=updated_desc&state=opened&first_page_size=100) 🔄
  - Help build the CI/CD components catalog:
    - [CI/CD components](https://gitlab.com/components/) 📖
    - [Development guide](https://docs.gitlab.com/ee/development/cicd/components.html) 🔧

/cc @gitlab-community/maintainers
