# frozen_string_literal: true

require_relative 'gitlab_client_helper'

# A module included into the resource context to provide helper methods to the triage policies
module GitlabTriageResourceContextPlugin
  COMMUNITY_MEMBER_GROUP_ID = 61_840_430

  def close_access_request(user_id)
    if GitlabTriageEnginePlugin.dry_run?
      puts "Would close access request for user id: #{user_id}"
    else
      access_request_client.deny_group_access_request(COMMUNITY_MEMBER_GROUP_ID, user_id)
    end
  end

  def access_request_for_user(user_id)
    access_requests.find { |request| request.id == user_id }
  end

  private

  # rubocop:disable Style/ClassVars -- gitlab-triage instantiates a class for each resource and instance vars would cause a massive performance issue
  def access_requests
    @@access_requests ||= access_request_client.group_access_requests(COMMUNITY_MEMBER_GROUP_ID)
  end

  def access_request_client
    @@access_request_client ||= GitlabClientHelper.new(ENV.fetch('ACCESS_REQUESTS_GITLAB_API_TOKEN'))
  end
  # rubocop:enable Style/ClassVars
end

# A module prepended to the engine so we can extract state that would otherwise not be available
module GitlabTriageEnginePlugin
  # rubocop:disable Style/ClassVars -- we want to have dry-run available as global state
  def perform(*)
    @@dry_run = options.dry_run
    super
  end

  def self.dry_run?
    @@dry_run
  end
  # rubocop:enable Style/ClassVars
end

Gitlab::Triage::Engine.prepend GitlabTriageEnginePlugin
Gitlab::Triage::Resource::Context.include GitlabTriageResourceContextPlugin
