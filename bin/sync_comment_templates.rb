#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative '../lib/gitlab_graphql_helper'

# A class for syncing project comment templates from .gitlab/comment_templates
# Runs in a scheduled pipeline with default args and fails if there are mismatches
# Runs with REPORT_ONLY=1 in merge requests and reports mismatches without failing
# Runs with FORCE_SYNC=1 and DRY_RUN=0 when merging templates changes to the default branch
# Can be run manually at anytime with FORCE_SYNC=1 and DRY_RUN=0 to fix drift
class SyncCommentTemplates
  def initialize(project_id, project_path)
    token = ENV.fetch('SYNC_COMMENT_TEMPLATES_GITLAB_API_TOKEN')
    @graphql_helper = GitlabGraphqlHelper.new(token)
    @force_sync = ENV.fetch('FORCE_SYNC', '0') == '1'
    @report_only = ENV.fetch('REPORT_ONLY', '0') == '1'
    @dry_run = ENV.fetch('DRY_RUN', '1') != '0'
    @project_id = project_id
    @project_path = project_path
  end

  def project_templates
    query = <<~QUERY
      query {
        project(fullPath: "#{@project_path}") {
          savedReplies {
            nodes {
              id
              name
              content
            }
          }
        }
      }
    QUERY
    @project_templates ||= @graphql_helper.execute_graphql(query)
      .dig('data', 'project', 'savedReplies', 'nodes')
      .to_h { |template| [template['name'], { id: template['id'], content: template['content'] }] }
  end

  def create_project_template(name, content)
    query = <<~QUERY
      mutation {
        projectSavedReplyCreate(input: {
          projectId: "gid://gitlab/Project/#{@project_id}"
          name: "#{name}"
          content: """#{content.strip}"""
        }) {
          errors
        }
      }
    QUERY

    execute_mutation(query)
  end

  def destroy_project_template(id)
    query = <<~QUERY
      mutation {
        projectSavedReplyDestroy(input: {
          id: "#{id}"
        }) {
          errors
        }
      }
    QUERY

    execute_mutation(query)
  end

  def update_project_template(id, name, content)
    query = <<~QUERY
      mutation {
        projectSavedReplyUpdate(input: {
          id: "#{id}"
          name: "#{name}"
          content: """#{content.strip}"""
        }) {
          errors
        }
      }
    QUERY

    execute_mutation(query)
  end

  def execute_mutation(query)
    if @dry_run
      puts 'Would execute:'
      puts query
      puts '==='
    else
      @graphql_helper.execute_graphql(query)
    end
  end

  def disk_templates
    @disk_templates ||= Dir.glob(".gitlab/comment_templates/#{@project_id}/*.md").to_h do |file|
      [File.basename(file, '.md'), File.read(file)]
    end
  end

  def backup_project_templates
    File.write("#{@project_id}_exported_templates.json", JSON.pretty_generate(project_templates))
  end

  def execute
    mismatches = sync_templates

    handle_mismatches if mismatches.positive?
  end

  def sync_templates
    template_names = disk_templates.keys | project_templates.keys
    template_names.count { |template_name| sync_template(template_name) }
  end

  def sync_template(template_name)
    disk_template = disk_templates[template_name]
    project_template = project_templates[template_name]

    if project_template.nil?
      # A new template in .gitlab/comment_templates (or a project template was deleted)
      puts "Project template doesn't exist: #{template_name}"
      create_project_template(template_name, disk_template) if @force_sync
    elsif disk_template.nil?
      # A template created directly in the project (or a template deleted from .gitlab/comment_templates)
      puts "Disk template doesn't exist: #{template_name}"
      destroy_project_template(project_template[:id]) if @force_sync
    elsif project_template[:content].strip != disk_template.strip
      # A template changed in the project or in .gitlab/comment_templates
      puts "Template mismatch: #{template_name}"
      puts '===Project template:'
      puts project_template[:content].strip
      puts '===Disk template:'
      puts disk_template.strip
      puts '==='
      update_project_template(project_template[:id], template_name, disk_template) if @force_sync
    else
      puts "Template matches: #{template_name}"
      return false
    end

    true
  end

  def handle_mismatches
    if @force_sync
      backup_project_templates
    elsif !@report_only
      raise 'Project templates are out of sync with .gitlab/comment_templates. Run with FORCE_SYNC=1 to sync.'
    end
  end
end

if $PROGRAM_NAME == __FILE__
  PROJECTS = [
    { id: 60_607_268, path: 'gitlab-community/community-members/onboarding' },
    { id: 61_577_338, path: 'gitlab-community/community-members/duo-access' }
  ].freeze

  start = Time.now

  project_id = ENV.fetch('PROJECT_ID', nil)
  project_path = ENV.fetch('PROJECT_PATH', nil)

  if project_id && project_path
    SyncCommentTemplates.new(project_id, project_path).execute
  else
    PROJECTS.each do |project|
      puts "Syncing project: #{project[:path]}"
      SyncCommentTemplates.new(project[:id], project[:path]).execute
      puts '==='
    end
  end

  puts "Done in #{Time.now - start} seconds."
end
