#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'

require_relative '../lib/gitlab_client_helper'

# Create ~access-request issues for community fork access requests
class CommunityForksAccessRequests
  COMMUNITY_FORK_MEMBERS_GROUP_ID = 61_840_430
  COMMUNITY_FORK_META_PROJECT_ID = 41_650_567
  ONBOARDING_PROJECT_ID = 60_607_268

  def initialize
    token = ENV.fetch('ACCESS_REQUESTS_GITLAB_API_TOKEN')
    @gitlab_client = GitlabClientHelper.new(token)
    @dry_run = ENV.fetch('DRY_RUN', '1') != '0'
  end

  def onboarding_issues
    created_after = (Date.today - 10).iso8601
    @onboarding_issues ||= @gitlab_client.issues(ONBOARDING_PROJECT_ID, { created_after: })
  end

  def access_requests
    @access_requests ||= @gitlab_client.group_access_requests(COMMUNITY_FORK_MEMBERS_GROUP_ID)
  end

  def issue_description(username)
    @issue_template ||= @gitlab_client.repo_file(
      COMMUNITY_FORK_META_PROJECT_ID,
      '.gitlab/issue_templates/Onboarding.md'
    )

    @issue_template.gsub('USERNAME', "@#{username}")
  end

  def issue_title(username)
    "@#{username} contributor onboarding"
  end

  def create_new_onboarding_issues
    access_requests.each do |access_request|
      username = access_request.username

      if Date.parse(access_request.requested_at) < (Date.today - 7)
        puts "Access request for: #{username} occurred more than 7 days ago"
        next
      end

      if onboarding_issues.any? { |issue| issue.assignees.any? { |assignee| assignee.username == username } }
        puts "Access request issue already exists for: #{username}"
        next
      end

      title = issue_title(username)
      description = issue_description(username)

      if @dry_run
        puts "Would create issue for #{username} with title: #{title} and description:\n```\n#{description}\n```"
      else
        issue = @gitlab_client.create_issue(ONBOARDING_PROJECT_ID, title, description)
        puts "Issue created for #{username} with iid: #{issue.iid}"
      end
    end
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  service = CommunityForksAccessRequests.new
  service.create_new_onboarding_issues

  puts '==='
  puts "Done in #{Time.now - start} seconds."
end
