#!/usr/bin/env ruby
# frozen_string_literal: true

require 'date'

require_relative '../lib/gitlab_client_helper'

# Create ~access-request issues for community fork access requests
class UpdateOnboardingIssueLabel
  COMMUNITY_FORK_MAINTAINERS_GROUP_ID = 69_606_404
  ONBOARDING_PROJECT_ID = 60_607_268

  def initialize
    token = ENV.fetch('UPDATE_ONBOARDING_ISSUE_LABEL_GITLAB_API_TOKEN')
    @gitlab_client = GitlabClientHelper.new(token)
    @dry_run = ENV.fetch('DRY_RUN', '1') != '0'
  end

  def maintainers
    @maintainers ||= @gitlab_client.group_members(COMMUNITY_FORK_MAINTAINERS_GROUP_ID).map(&:id)
  end

  def assignee
    @assignee ||= ENV.fetch('ISSUE_ASSIGNEE_ID').to_i
  end

  def commenter
    @commenter ||= ENV.fetch('ISSUE_COMMENTER_ID').to_i
  end

  def issue_iid
    @issue_iid ||= ENV.fetch('ISSUE_IID')
  end

  def execute
    puts "assignee: #{assignee}, commenter: #{commenter}, issue_iid: #{issue_iid}" if @dry_run

    if commenter == assignee
      puts 'Assignee commented, adding ~updated label'
      return if @dry_run

      return @gitlab_client.create_issue_note(ONBOARDING_PROJECT_ID, issue_iid, '/label ~updated')
    end

    puts "maintainers: #{maintainers.join(', ')}" if @dry_run
    return unless maintainers.include?(commenter)

    puts 'Maintainer commented, removing ~updated label'
    return if @dry_run

    @gitlab_client.create_issue_note(ONBOARDING_PROJECT_ID, issue_iid, '/unlabel ~updated')
  end
end

if $PROGRAM_NAME == __FILE__
  start = Time.now

  UpdateOnboardingIssueLabel.new.execute

  puts '==='
  puts "Done in #{Time.now - start} seconds."
end
